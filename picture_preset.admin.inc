<?php

/**
 * Page callback with preset overview.
 */
function picture_preset_preset_list() {
  // Initialize page and rows arrays.
  $page = array();

  // Set custom title.
  drupal_set_title('Picture Presets');

  // Define introduction.
  $page['picture_preset_intro'][] = array('#markup' => t('<p>Use <code>hook_picture_preset_info()</code> to define and <code>hook_picture_preset_info_alter()</code> to alter picture presets. For example implementation of these hooks, see <code>picture_preset.api.php</code>.</p>'));
  $page['picture_preset_intro'][] = array('#markup' => t("<p>When picture presets have not been cached, Picture Preset invokes other modules to provide picture presets. Image styles will automatically be created for each picture preset's breakpoint. Image styles will be flushed for altered or deleted picture presets.</p>"));

  $page['picture_preset_intro'][] = array('#markup' => t('<p>NB: Remember to always clear (Picture Preset) caches after adding, altering or deleting picture presets!</p>'));

  // Add settings form.
  $page['settings'] = drupal_get_form('picture_preset_admin_form');

  // Iterate through presets to create rows.
  $rows = array();
  foreach (picture_preset_definitions() as $preset_key => $preset) {
    $row = array();
    $row[] = $preset['title'];
    $row[] = l(t('Flush image styles'), PICTURE_PRESET_ADMIN_PATH . '/' . $preset_key . '/flush-image-styles');
    $rows[] = $row;
  }

  // If no presets have been defined yet.
  if (empty($rows)) {
    // Add row with empty message.
    $rows[] = array(
      array(
        'data' => t('There are currently no presets.'),
        'colspan' => 2,
      )
    );
  }

  // Return table.
  $page['picture_preset_overview'] = array(
    '#theme' => 'table',
    '#caption' => t('Flush image styles so images will be regenerated the next time they are requested.'),
    '#header' => array(t('Preset name'), t('Operations')),
    '#rows' => $rows,
  );

  return $page;
}

/**
 * Flush image styles confirmation form.
 */
function picture_preset_flush_image_styles_confirmation_form($form, &$form_state) {
  // Get preset key from arguments. The last argument is the action, the former
  // last is the preset key.
  $args = arg();
  $preset_key = $args[count($args) - 2];
  // Get presets.
  $presets = picture_preset_definitions();

  // If the preset exists.
  if (isset($presets[$preset_key])) {
    // Set preset key as form value.
    $form['preset_key'] = array(
      '#type' => 'value',
      '#value' => $preset_key,
    );

    return confirm_form(
      $form,
      t('Are you sure you want to flush image styles for picture preset %title?', array('%title' => $presets[$preset_key]['title'])),
      PICTURE_PRESET_ADMIN_PATH,
      t('This action cannot be undone.'),
      t('Flush image styles'),
      t('Cancel')
    );
  }
  else {
    // Output warning.
    drupal_set_message(t('Preset %preset_key does not exist.', array('%preset_key' => $preset_key)), 'error');
    // Redirect to overview.
    drupal_goto(PICTURE_PRESET_ADMIN_PATH);
  }
}

/**
 * Flush image styles confirmation form submit handler.
 */
function picture_preset_flush_image_styles_confirmation_form_submit($form, &$form_state) {
  // Get presets.
  $presets = picture_preset_definitions();
  // Get preset key from form values.
  $preset_key = $form_state['values']['preset_key'];

  // If the preset exists.
  if (isset($presets[$preset_key])) {
    // Iterate through breakpoints.
    foreach ($presets[$preset_key]['breakpoints'] as $breakpoint_key => $breakpoint) {
      // Define directory URI.
      picture_preset_image_style_flush(picture_preset_create_image_style_key($preset_key, $breakpoint_key));
    }
    // Output message.
    drupal_set_message(t('Image styles have successfully been flushed for preset %title.', array('%title' => $presets[$preset_key]['title'])));
  }

  // Redirect to overview.
  $form_state['redirect'] = PICTURE_PRESET_ADMIN_PATH;
}

/**
 * Settings form.
 */
function picture_preset_admin_form($form, &$form_state) {
  // Always load this include when the form is being built.
  form_load_include($form_state, 'picture_preset', 'inc', 'picture_preset.admin');

  // Add checkbox to temporarily disable Picture Preset cache.
  $form['picture_preset_disable_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Picture Preset caching'),
    '#default_value' => variable_get('picture_preset_disable_cache', FALSE),
    '#description' => t('When preset cache is disabled, Picture Preset will check on each page call whether presets have been added, altered or deleted.'),
  );

  // Submit button to save and rebuild cache.
  $form['actions']['rebuild_picture_preset_cache'] = array(
    '#type' => 'submit',
    '#name' => 'rebuild_picture_preset_cache',
    '#value' => t('Save and rebuild Picture Preset cache'),
    '#weight' => 10,
  );

  // Add submit callback.
  $form['#submit'][] = 'picture_preset_admin_form_submit';

  // Return settings form.
  return system_settings_form($form);
}

/**
 * Submit callback for settings form.
 */
function picture_preset_admin_form_submit($form, &$form_state) {
  // If a trigger element has been defined.
  if (isset($form_state['triggering_element']['#name'])) {
    // If trigger element is "Save and rebuild Picture Preset cache" button.
    if ($form_state['triggering_element']['#name'] === 'rebuild_picture_preset_cache') {
      // Get rebuilt presets.
      picture_preset_definitions(TRUE);
      // Output message.
      drupal_set_message(t('Picture Preset cache has been rebuilt.'));
    }
  }
}
