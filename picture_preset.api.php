<?php

/**
 * Implements hook_picture_preset_info().
 *
 * @return array
 *   An array with picture presets.
 */
function hook_picture_preset_info() {
  $presets = array();

  // Viewport width: 100% wide and scaled.
  $presets['vw_100p_scaled'] = array(
    // Define a human readable title for this effect.
    'title' => '100% wide scaled',
    // Define a set of breakpoints.
    'breakpoints' => array(
      // The key of a breakpoint can be anything.
      'sm' => array(
        // Define the media query.
        'media' => '(max-width: 30em)',
        // Define an array with image effects.
        'effects' => array(
          // Add a scale image effect. For default image effects:
          // @link https://api.drupal.org/api/drupal/modules%21image%21image.effects.inc/7
          'image_scale_effect' => array('width' => 480, 'height' => 999),
        ),
      ),
      'md' => array(
        'media' => '(max-width: 48em)',
        'effects' => array(
          'image_scale_effect' => array('width' => 768, 'height' => 999),
        ),
      ),
      'md_plus' => array(
        'media' => '(max-width: 60em)',
        'effects' => array(
          // Use weights to change image effects' order.
          'image_scale_effect' => array(
            'width' => 960,
            'height' => 999,
            'weight' => 20,
          ),
          'image_crop_effect' => array(
            'width' => 600,
            'height' => 600,
            'anchor' => 'center-center',
            'weight' => 10,
          ),
        ),
      ),
      'lg' => array(
        'media' => '(max-width: 75em)',
        'effects' => array(
          'image_scale_effect' => array('width' => 1200, 'height' => 999),
        ),
      ),
      'lg_plus' => array(
        'media' => '(max-width: 999em)',
        'effects' => array(
          'image_scale_effect' => array('width' => 1920, 'height' => 999),
        ),
      ),
    ),
  );

  return $presets;
}

/**
 * Implements hook_picture_preset_info_alter().
 *
 * @param array
 *   An array with picture presets.
 */
function hook_picture_preset_info_alter(&$presets) {
  // Change an existing media query.
  $presets['vw_100p_scaled']['breakpoints']['sm']['media'] = '(max-width: 35em)';
  // Add an image effect to an existing media query.
  $presets['vw_100p_scaled']['breakpoints']['sm']['media']['effects']['image_crop_effect'] = array(
    'width' => 300,
    'height' => 300,
    'anchor' => 'center-center',
  );
}
